<?php
function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

class Radio {
  private $path = [];
  private $duration = [];
  private $title = [];
  private $poster = [];
  private $tracklist = [];
  private $default_poster;
  private $current_index;
  private $current_pos_sum;
  private $current_pos;
  private $qtd;
  
  function __construct($posterG, $posterR18, $r18) {
    $this->default_poster = !$r18 ? $posterG : $posterR18;
  }
  
  private function duration_to_sec($duration){
	$ex = explode(':', $duration);
    $last_value = count($ex) - 1;
    $h = empty($ex[$last_value - 2]) ? 0 : $ex[$last_value - 2];
    $m = $ex[$last_value - 1];
    $s = $ex[$last_value];
	$totseg = $h*60*60 + $m*60 + $s;
	return $totseg;
  }

  function add($p, $d, $title, $poster, $tracklist){
	$totseg = $this->duration_to_sec($d);
	array_push($this->duration, $totseg);
	array_push($this->path, $p);
	array_push($this->title, $title);
	array_push($this->poster, $poster);
	
	$tl = explode(PHP_EOL, file_get_contents($tracklist));
	$title = [];
	$timestamp = [];
	
	if(!is_null($tracklist)){
		foreach($tl as $t){
		  $track = explode(' ', $t, 2);
		  
		  array_push($timestamp, $this->duration_to_sec($track[0]));
		  array_push($title, htmlentities(trim($track[1]), ENT_QUOTES)); 
		}
		array_push($this->tracklist, array($timestamp, $title));
	}else{
		array_push($this->tracklist, false);
	}

  }
  
  private function sum_time(){
	$sum = 0;
    foreach($this->duration as $d){
	  $sum += $d;
	}
	return $sum;
  }

  private function get_pos_sum() {
	if($this->current_pos_sum == ''){
      $current_time = date("h")*60*60 + date("i")*60 + date("s");
	  $this->current_pos_sum = $current_time % $this->sum_time();
    }
    return $this->current_pos_sum;
  }
  
  private function calc_index_pos(){
	$pos = $this->get_pos_sum();
	$sum = 0;

	for ($i = 0; $i < count($this->duration); $i++)  {
	  $sum += $this->duration[$i];
      if($pos <= $sum){
	    $this->current_index = $i;
		$current_pos = $pos - ($sum - $this->duration[$i]);
		$this->current_pos = $current_pos;
		break;
	  }
    }
  }
  
  private function current_index(){
    if($this->current_index == ''){
	  $this->calc_index_pos();
	}
	return $this->current_index;
  }
  
  private function get_pos(){
    if($this->current_pos == ''){
	  $this->calc_index_pos();
	}
	return $this->current_pos;
  }
    
  function get_url() {
	
	if($this->current_index == '' || $this->current_pos == ''){
	  $this->calc_index_pos();
	}
	
	$url = $this->path[$this->current_index].'#t='.$this->current_pos;
	return $url;
  }
  
  
  function get_index(){
	return $this->current_index();
  }
  function count(){
	return count($this->path);
  }
  function get_path($p){
    return $this->path[$p];
  }
  function get_current_title(){
	$p = $this->current_index();
	$title = $this->title[$p];
    return $this->title[$p];
  }
  function get_current_track(){  
	$index = $this->current_index();
	if(!$this->tracklist[$index]) return false;
	
	$pos = $this->get_pos();
	
	$sum = 0;
	$track_index = count($this->tracklist[$index][0]) - 1;
	
	for($i = 0; $i < count($this->tracklist[$index][0]); $i++){
      if($this->tracklist[$index][0][$i] < $pos){
		$track_index = $i;
	  }
	}
	return $this->tracklist[0][1][$track_index];
  }
  function get_current_poster(){
	$p = $this->current_index();
	$poster = isset($this->poster[$p]) ? $this->poster[$p] : $this->default_poster;
    return $poster;
  }
  function get_title($p){
    return $this->title[$p];
  }
  function get_poster($p){
    $poster = isset($this->poster[$p]) ? $this->poster[$p] : $this->default_poster;
	return $poster;
  }
  function get_tracklist($p){
	$tracklist_json = json_encode($this->tracklist[$p][1]);
	return $tracklist_json;
  }
   function get_tracklist_time($p){
	$tracklist_json = json_encode($this->tracklist[$p][0]);
	return $tracklist_json;
  }

}
function nomeArtista ($nome_arquivo){
	$exp = explode('__',$nome_arquivo);
	return empty($exp[1]) ? 'Sei lá' : $exp[1];
}

function cropOpt ($nome_arquivo){
	$exp = explode('__',$nome_arquivo);
	$exp2 = explode('.',$exp[2]);
	if($exp2[0] == ''){
	  $opt = '1';
	}else{
	  $opt = $exp2[0];
	}
	return $opt;
}

function epoch ($nome_arquivo){
	$exp = explode('__',$nome_arquivo);
	return $exp[0];
}
function addURLParameter($url, $paramName, $paramValue) {
     $url_data = parse_url($url);
     if(!isset($url_data["query"]))
         $url_data["query"]="";

     $params = array();
     parse_str($url_data['query'], $params);
     $params[$paramName] = $paramValue;
     $url_data['query'] = http_build_query($params);
     return build_url($url_data);
}

 function build_url($url_data) {
     $url="";
     if(isset($url_data['host']))
     {
         $url .= $url_data['scheme'] . '://';
         if (isset($url_data['user'])) {
             $url .= $url_data['user'];
                 if (isset($url_data['pass'])) {
                     $url .= ':' . $url_data['pass'];
                 }
             $url .= '@';
         }
         $url .= $url_data['host'];
         if (isset($url_data['port'])) {
             $url .= ':' . $url_data['port'];
         }
     }
     $url .= $url_data['path'];
     if (isset($url_data['query'])) {
         $url .= '?' . $url_data['query'];
     }
     if (isset($url_data['fragment'])) {
         $url .= '#' . $url_data['fragment'];
     }
     return $url;
 }
?>