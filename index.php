<!DOCTYPE html>
<?php
date_default_timezone_set('America/Sao_Paulo');
include("phpThumb/phpThumb.config.php");
include("functions.php");

$pastaNormal = 'ilusts';
$pastaR18 = 'ilustsR18';
$default_poster = 'radio/default_poster.png';
$default_posterR18 = 'radio/default_posterR18.png';
$gifa = true;
$index_start = isset($_GET["index"]) ? $_GET["index"] : 0;
$r18 = isset($_GET["R18"]) ? true : false;

include("radio_rules.php");
$radio = new Radio($default_poster, $default_posterR18, $r18);
$playlist = json_decode($playlist_file);

foreach($playlist as $p){
  $radio->add($p->path,$p->duration,$p->title,$p->poster, $p->tracklist);
}

$pathIlust = !$r18 ? $pastaNormal : $pastaR18;

$files = array_values(array_filter(scandir($pathIlust ,1), function($item) use($pathIlust) {
    return !is_dir($pathIlust.'/'. $item);
}));

$lastEpoch = epoch($files[0]);
$lastDate = date('D, d/m/Y h:i a',$lastEpoch);
?>
<html>
<head>
  <meta charset="utf-8">
  <title>Central da Arte 2D<?php if($r18) echo ' | R-18'; ?></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/component.css">
	<link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<div class="wrap-body">
<!--------------Header--------------->
<div class="top-header">
	<div class="zerogrid">
		<div>
			<nav>
			  <a class="toggleMenu" href="#">Menu</a>
			  <ul class="menu">
				  <li><a href="https://yggbrasil.neocities.org/">YggBrasil</a></li>
				  
				  <li><a class="new" style="font-size: small; padding-left:5px; padding-right:5px" href="http://[201:2a0e:fc25:a58c:6705:e448:9312:16d4]:5000/b/">LeilaChan</a></li>
				  <li><a style="font-size: small; padding-left:5px; padding-right:5px; color:white">|</a></li>
				  
				  <li><a style="font-size: small; padding-left:5px; padding-right:5px" href="http://[202:d702:6f4:60c9:9b84:d233:8c07:5e10]">O Esconderijo</a></li>

				  <li><a style="font-size: small; padding-left:5px; padding-right:5px; color:white">|</a></li>
                  <li><a style="font-size: small; padding-left:5px; padding-right:5px" href="http://[201:3069:5b74:5583:207f:a16f:e355:9cdc]"/>Site de um Anônimo</a></li>


				  <li><a style="font-size: small; padding-left:5px; padding-right:5px; color:white">|</a></li>
				  <li><a style="font-size: small; padding-left:5px; padding-right:5px" href="http://[324:71e:281a:9ed3::41]/web/#yggbrasil-irc">YggBrasil IRC</a></li>
			  </ul>
			</nav>
		</div>
	</div>
	<div class="r18-btn"><?php echo $r18?'<a href=".">G</a>':'<a href="?R18">R18+</a>';?></div>
</div>

<header>
	<div class="wrap-header">
		<div class="zerogrid">
			<img src="images/title.png" alt="Central da Arte 2D" class="titulo" usemap="#titulo">
			<map name="titulo">
				<area shape="rect" coords="60,30,420,83" alt="Computer" href="\<?php if($r18) echo "?R18"; ?>">
			</map>
			<p class="subtitulo">イラスト</p>
		</div>
		</header>
	</div>
<?php if ($enable_radio) { ?>
<div class="radio">
  <div class="label">
    <center><p id="radio_title"><?php echo $radio->get_current_title(); ?></p></center>
    <center><marquee <?php if(!$radio->get_current_track())echo 'style="opacity:0; height:0; margin-top:0;"';?> id="now_playing" scrollamount="3" behavior="alternate">♫ <?php echo $radio->get_current_track(); ?> ♫</marquee></center>
  </div>
  <center>
  
  <?php if($cover){ ?>
  <div>
    <img id="radio_poster" width="289" height="180" src="<?php echo $radio->get_current_poster(); ?>" />
  </div>
  <?php } ?>
  <video class="audio" id="media" autoplay controls muted>
    <source src="<?php echo $radio->get_url(); ?>"
  </video>
  </center>
  	<script>
	var vid = document.getElementById("media");
	var radio_title = document.getElementById("radio_title");
	var now_playing = document.getElementById("now_playing");
	var count = 0;
    var src = [];	
	var poster = [];
	var title = [];
	var tracklist = [];
	var tracklist_time = [];
	<?php 
	 for($i=0;$i < $radio->count();$i++){
		 $src = $radio->get_path($i);
		 $title = $radio->get_title($i);
		 $poster = $radio->get_poster($i);
		 
		   $tracklist = $radio->get_tracklist($i);
		   $tracklist_time = $radio->get_tracklist_time($i);
		   echo "tracklist.push(JSON.parse('$tracklist'));";
		   echo "tracklist_time.push(JSON.parse('$tracklist_time'));";
		 
		 echo "src.push('$src');";
		 echo "title.push('$title');";
		 echo "poster.push('$poster');";
		 
	 }
	?>;
	var actual = <?php echo $radio->get_index(); ?>;
	var current = actual;
	vid.onended = function() {
		count++;
		var next = count + actual;
		if(next >= src.length){
			actual = 0;
			next = 0;
			count = 0;
		}
		vid.src = src[next];
		vid.poster = poster[next];
		radio_title.innerHTML = title[next];
		if(tracklist[next] == null){
		  now_playing.style.opacity = "0";
		  now_playing.style.height = "0";
		  now_playing.style.marginTop = "0";
		}else{
		  now_playing.style.opacity = "1";
		  now_playing.style.height = "initial";
		  now_playing.style.marginTop = "3px";
		}
		current = next;
	};
	var intervalId = window.setInterval(function(){
	   var track_index;
	   
	   if(tracklist[current] != null){	   
	     for (var i = 0; i < tracklist_time[current].length; i++) {
           if(tracklist_time[current][i] < vid.currentTime){
		  	 track_index = i;
		   }
         }
		 let track = tracklist[current][track_index];
		 if(typeof track != "undefined"){
	       now_playing.innerHTML = "♫ " + track + " ♫";
		 }
	   }
	   
    }, 1000);
    </script>
</div>
<?php } ?>
<!--------------Content--------------->
<section class="container">
  <?php if($dancer){ ?>
  <img id="dançadeira" src="images/<?php echo(!$r18?'lum2_small.gif':'lum2_r_small'); ?>" />
  <?php } ?>
	<div class="zerogrid">
		<div class="col">
			<div id="main-content">
				<div class="row">
					<div class="col">
						<h1 class="titulo2 row">
							<span class="col-sm"></span>
							<span class="col-sm">Ilustrações recentes<?php if($r18) echo '<span class="r18">R-18</span>'; ?></span>
							<span class="last col-sm">(Last update: <?php echo $lastDate; ?>)</span>
						</h1>
					</div>
				</div>
				<div class="row ilust-rec">
				<a class="previous" href="<?php echo addURLParameter($_SERVER['REQUEST_URI'], "index", $index_start+5);?>" title="5 anteriores">&lt;</a>
				<?php if($index_start > 0){ ?><a class="next" href="<?php echo addURLParameter($_SERVER['REQUEST_URI'], "index", $index_start-5);?>" title="5 próximos">&gt;</a><?php } ?>
				<?php for($i = $index_start; $i < $index_start+5; ++$i) {
				if($gifa){
					$params = pathinfo($files[$i], PATHINFO_EXTENSION) === "gif" ? '&q=70&f=gif' : '&q=90';
				}else{
					$params = '&q=90';
				}
				?>
					<div class="celRec">
						<div class="wrap-col">
							<article>
								<a <?php echo'href="'.$pathIlust.'/'.$files[$i].'"';?>><img loading="lazy" width="252" height="315" class="full" src="<?php if(isset($files[$i])){
									echo htmlspecialchars(phpThumbURL('src=/'.$pathIlust.'/'.$files[$i].'&w=252&&h=315&zc='.cropOpt($files[$i]).$params, 'phpThumb/phpThumb.php'));
									}else{
									  echo "images/acabou.gif";
									}?>"></a>

								<div class="wrap-art">
									<div class="art-header">
										<h1 class="title"><?php if(isset($files[$i])){ echo '<b>Artista</b>: '.nomeArtista($files[$i]);}else{ echo "Não tem mais.";}?></h1>
									</div>
								</div>
							</article>
						</div>
					</div>
				<?php } ?>
				</div>
				<div style="margin-bottom:2em"></div>
				<div class="row">
					<div class="col">
						<h1 class="titulo2"><span>Outras ilustrações</span><?php if($r18) echo '<span class="r18">R-18</span>'; ?></h1>
					</div>
				</div>
				<div class="row outras-ilust">
				<?php
				for ($index = $index_start+5; $index < sizeof($files); ++$index) {
				$isGif = pathinfo($files[$index], PATHINFO_EXTENSION) === "gif" ? true : false;
				if($gifa){
					$params = $isGif ? '&q=70&f=gif' : '&q=90';
				}else{
					$params = '&q=90';
				}
				?>
					<div class="cellAll">
						<div class="wrap-col">
							<article class="thumbs">
								<a <?php echo 'href="'.$pathIlust.'/'.$files[$index].'" ';if($isGif) echo "class='gif'" ?> >
								<img class="thumb" loading="lazy" width="160" height="200" src="<?php echo htmlspecialchars(phpThumbURL('src=/'.$pathIlust.'/'.$files[$index].'&w=160&&h=200&zc='.cropOpt($files[$index]).'&q=90', 'phpThumb/phpThumb.php'))?>">
								<?php if($isGif) { ?><img class="thumb quegifa" src="<?php echo htmlspecialchars(phpThumbURL('src=/'.$pathIlust.'/'.$files[$index].'&w=160&&h=200&zc=1'.$params, 'phpThumb/phpThumb.php'))?>"><?php } ?>
								</a>
							</article>
						</div>
					</div>
				<?php } ?>
				</div>

		</div>

	</div>
</section>
<!--------------Footer--------------->
<footer>
	<div class="zerogrid">
	   <div class="col-">
			<div class="copyright">
				@ 2022 by anão. | Bateria: <?php echo trim(file_get_contents("/sys/class/power_supply/battery/capacity"))."% (".trim(file_get_contents("/sys/class/power_supply/battery/status")).")"; ?>
			</div>
	   </div>
	</div>
	   <div>
	   </div>

	</div>
</footer>

</div>

</body>

</html>
