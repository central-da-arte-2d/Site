<?php
///////////////////////////////////////////
// Put conditions to play the radio here //
///////////////////////////////////////////

// The playlist file must be in JSON format, where each object represents a media file, expecting the following fields:
//   path: [the path of the media file]
//   duration: [the duration of the media file formated as h:m:s or m:s]
//   title: [title of the media showing in the top of the radio]
//   poster: [path to the poster image] (optional)
//   tracklist: [path to the tracklist file, if the media contains many tracks] (optional)
//     tracklist format:
//      [h:m:s] [track title1]
//      [h:m:s] [track title2]
//      ...
$playlist_file = file_get_contents('radio/playlist.json');
$enable_radio = false; // enables the radio
$cover = false; //shows the media poster if true, if the image is not set in the JSON object it defaults to default_poster.png
$dancer = false; //shows lum dancing
?>